FRITZ!Box PHP
=============

Installation
------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this library:

.. code-block:: bash

    $ composer require fritzbox/fritzbox-php

This command requires you to have Composer installed globally, as explained
in the `installation chapter`_ of the Composer documentation.

Example
-------

Create a new instance of FritzBox and call an available action on it.

.. code-block:: php

    <?php
    // example.php

    $fritzbox = new \FritzBox\FritzBox('fritz.box', 'your-secret-password', 'your-optional-username');

    // create an action
    $getPhonebook = new \FritzBox\Action\GetPhonebook();

    // get the phonebook
    $phonebook = $fritzbox->call($getPhonebook);

    // otherwise, get the device with all available devices and services
    $device = $fritzbox->getDevice();

Contribute
----------

You can fork the repository and create/extend the actions you need (which must implement the ActionInterface).
After that, create a pull request and i will merge it to this repository.

.. _`installation chapter`: https://getcomposer.org/doc/00-intro.md
