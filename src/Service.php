<?php

namespace FritzBox;

class Service
{
    /**
     * @var string
     */
    protected $serviceType;

    /**
     * @var string
     */
    protected $serviceId;

    /**
     * @var string
     */
    protected $controlURL;

    /**
     * @var string
     */
    protected $eventSubURL;

    /**
     * @var string
     */
    protected $SCPDURL;

    /**
     * @return string
     */
    public function getServiceType(): string
    {
        return $this->serviceType;
    }

    /**
     * @param string $serviceType

     * @return $this
     */
    public function setServiceType(string $serviceType): self
    {
        $this->serviceType = $serviceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getServiceId(): string
    {
        return $this->serviceId;
    }

    /**
     * @param string $serviceId
     *
     * @return $this
     */
    public function setServiceId(string $serviceId): self
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * @return string
     */
    public function getControlURL(): string
    {
        return $this->controlURL;
    }

    /**
     * @param string $controlURL
     *
     * @return $this
     */
    public function setControlURL(string $controlURL): self
    {
        $this->controlURL = $controlURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventSubURL(): string
    {
        return $this->eventSubURL;
    }

    /**
     * @param string $eventSubURL
     *
     * @return $this
     */
    public function setEventSubURL(string $eventSubURL): self
    {
        $this->eventSubURL = $eventSubURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getSCPDURL(): string
    {
        return $this->SCPDURL;
    }

    /**
     * @param string $SCPDURL
     *
     * @return $this
     */
    public function setSCPDURL(string $SCPDURL): self
    {
        $this->SCPDURL = $SCPDURL;

        return $this;
    }
}
