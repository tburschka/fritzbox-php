<?php

namespace FritzBox;

class Device
{
    /**
     * @var string
     */
    protected $deviceType;

    /**
     * @var string
     */
    protected $friendlyName;

    /**
     * @var string
     */
    protected $manufacturer;

    /**
     * @var string
     */
    protected $manufacturerURL;

    /**
     * @var string
     */
    protected $modelDescription;

    /**
     * @var string
     */
    protected $modelName;

    /**
     * @var string
     */
    protected $modelNumber;

    /**
     * @var string
     */
    protected $modelURL;

    /**
     * @var string
     */
    protected $UDN;

    /**
     * @var \FritzBox\ServiceList
     */
    protected $serviceList;

    /**
     * @var \FritzBox\DeviceList
     */
    protected $deviceList;

    /**
     * @return string
     */
    public function getDeviceType(): string
    {
        return $this->deviceType;
    }

    /**
     * @param string $deviceType
     *
     * @return $this
     */
    public function setDeviceType(string $deviceType): self
    {
        $this->deviceType = $deviceType;

        return $this;
    }

    /**
     * @return string
     */
    public function getFriendlyName(): string
    {
        return $this->friendlyName;
    }

    /**
     * @param string $friendlyName
     *
     * @return $this
     */
    public function setFriendlyName(string $friendlyName): self
    {
        $this->friendlyName = $friendlyName;

        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturer(): string
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     *
     * @return $this
     */
    public function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return string
     */
    public function getManufacturerURL(): string
    {
        return $this->manufacturerURL;
    }

    /**
     * @param string $manufacturerURL
     *
     * @return $this
     */
    public function setManufacturerURL(string $manufacturerURL): self
    {
        $this->manufacturerURL = $manufacturerURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelDescription(): string
    {
        return $this->modelDescription;
    }

    /**
     * @param string $modelDescription
     *
     * @return $this
     */
    public function setModelDescription(string $modelDescription): self
    {
        $this->modelDescription = $modelDescription;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelName(): string
    {
        return $this->modelName;
    }

    /**
     * @param string $modelName
     *
     * @return $this
     */
    public function setModelName(string $modelName): self
    {
        $this->modelName = $modelName;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelNumber(): string
    {
        return $this->modelNumber;
    }

    /**
     * @param string $modelNumber
     *
     * @return $this
     */
    public function setModelNumber(string $modelNumber): self
    {
        $this->modelNumber = $modelNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelURL(): string
    {
        return $this->modelURL;
    }

    /**
     * @param string $modelURL
     *
     * @return $this
     */
    public function setModelURL(string $modelURL): self
    {
        $this->modelURL = $modelURL;

        return $this;
    }

    /**
     * @return string
     */
    public function getUDN(): string
    {
        return $this->UDN;
    }

    /**
     * @param string $UDN
     *
     * @return $this
     */
    public function setUDN(string $UDN): self
    {
        $this->UDN = $UDN;

        return $this;
    }

    /**
     * @return ServiceList
     */
    public function getServiceList(): ServiceList
    {
        return $this->serviceList;
    }

    /**
     * @param ServiceList $serviceList
     *
     * @return $this
     */
    public function setServiceList(ServiceList $serviceList): self
    {
        $this->serviceList = $serviceList;

        return $this;
    }

    /**
     *
     * @return DeviceList
     */
    public function getDeviceList(): DeviceList
    {
        return $this->deviceList;
    }

    /**
     * @param DeviceList $deviceList
     *
     * @return $this
     */
    public function setDeviceList(DeviceList $deviceList): self
    {
        $this->deviceList = $deviceList;

        return $this;
    }
}
