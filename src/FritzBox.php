<?php

namespace FritzBox;

use FritzBox\Action\ActionInterface;

class FritzBox
{
    const SCHEME = 'http';
    const PORT = 49000;
    const TR64_DESC = '/tr64desc.xml';

    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $pass;

    /**
     * @var string|null
     */
    protected $user;

    /**
     * @var Device
     */
    protected $device;

    /**
     * @param string      $host
     * @param string      $pass
     * @param string|null $user
     */
    public function __construct(string $host, string $pass, string $user = null)
    {
        $this->host = $host;
        $this->pass = $pass;
        $this->user = $user;
    }

    /**
     * @return Device
     */
    public function getDevice(): Device
    {
        if (!$this->device) {
            $this->device = $this->buildStructure();
        }

        return $this->device;
    }

    /**
     * @param ActionInterface $action
     *
     * @return mixed
     */
    public function call(ActionInterface $action)
    {
        $client = $this->getClient($action->getUrn(), $action->getUrl());

        $arguments = [];
        if (!empty($action->getArguments())) {
            foreach ($action->getArguments() as $key => $value) {
                $arguments[] = new \SoapParam($value, $key);
            }
        }

        $response = $client->__soapCall($action->getName(), $arguments);

        return $action->handleResponse($response);
    }

    /**
     * @param string $urn
     * @param string $url
     *
     * @return \SoapClient
     */
    public function getClient(string $urn, string $url): \SoapClient
    {
        $client = new \SoapClient(
            null,
            [
                'location' => $this->buildUrl($url),
                'uri' => $urn,
                'noroot' => true,
                'login' => $this->user,
                'password' => $this->pass,
                'trace' => true,
                'exceptions' => false
            ]
        );

        return $client;
    }

    /**
     * @param string $url
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function curl(string $url): string
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($handle);
        curl_close($handle);

        if ($output === false) {
            throw new \RuntimeException();
        }

        return $output;
    }

    /**
     * @return Device
     */
    protected function buildStructure(): Device
    {
        $data = $this->curl($this->buildUrl(self::TR64_DESC));
        $xml = simplexml_load_string($data);
        return $this->buildDevice($xml->device);
    }

    /**
     * @param \SimpleXMLElement $element
     *
     * @return Device
     */
    protected function buildDevice(\SimpleXMLElement $element): Device
    {
        $serviceList = new ServiceList();
        if (isset($element->serviceList)) {
            foreach ($element->serviceList->service as $service) {
                $serviceList->append($this->buildService($service));
            }
        }

        $deviceList = new DeviceList();
        if (isset($element->deviceList)) {
            foreach ($element->deviceList->device as $device) {
                $deviceList->append($this->buildDevice($device));
            }
        }

        return (new Device())
            ->setDeviceType($element->deviceType)
            ->setFriendlyName($element->friendlyName)
            ->setManufacturer($element->manufacturer)
            ->setManufacturerURL($element->manufacturerURL)
            ->setModelDescription($element->modelDescription)
            ->setModelName($element->modelName)
            ->setModelNumber($element->modelNumber)
            ->setModelURL($element->modelURL)
            ->setUDN($element->UDN)
            ->setServiceList($serviceList)
            ->setDeviceList($deviceList);
    }

    /**
     * @param \SimpleXMLElement $element
     *
     * @return Service
     */
    protected function buildService(\SimpleXMLElement $element): Service
    {
        return (new Service())
            ->setServiceType($element->serviceType)
            ->setServiceId($element->serviceId)
            ->setControlURL($element->controlURL)
            ->setEventSubURL($element->eventSubURL)
            ->setSCPDURL($element->SCPDURL);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function buildUrl(string $path): string
    {
        return self::SCHEME . '://' . $this->host . ':' . self::PORT . $path;
    }
}
