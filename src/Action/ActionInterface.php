<?php

namespace FritzBox\Action;

interface ActionInterface
{
    /**
     * @return string
     */
    public function getUrn(): string;

    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return array
     */
    public function getArguments(): array;

    /**
     * @param mixed $response
     *
     * @return mixed
     */
    public function handleResponse($response);
}
