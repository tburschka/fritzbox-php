<?php

namespace FritzBox\Action;

class GetPhonebook implements ActionInterface
{
    /**
     * @return string
     */
    public function getUrn(): string
    {
        return 'urn:dslforum-org:service:X_AVM-DE_OnTel:1';
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return '/upnp/control/x_contact';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'GetPhonebook';
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return ['NewPhonebookID' => 0];
    }

    /**
     * @param array $response
     * @return array
     */
    public function handleResponse($response)
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $response['NewPhonebookURL']);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        $xml = simplexml_load_string(curl_exec($handle));
        curl_close($handle);

        $entries = [];
        foreach ($xml->phonebook->contact as $contact) {
            $numbers = [];
            foreach ($contact->telephony->number as $number) {
                $numbers[] = [
                    'type' => (string) $number['type'],
                    'quickDial' => (string) $number['quickdial'],
                    'vanity' => (string) $number['vanity'],
                    'prio' => (int) $number['prio'],
                    'number' => (string) $number,
                ] ;
            }
            $entries[] = [
                'name' => (string) $contact->person->realName,
                'uniqueId' => (int) $contact->uniqueid,
                'category' => (int) $contact->category,
                'numbers'  => $numbers,
            ];
        }

        return $entries;
    }
}
